#pragma once
#include <vector>

class SVD_Algo
{
protected:

	size_t rows;
	size_t columns;
	std::vector <float> matrix;

public:
	virtual void perform_svd(std::vector<float> &U, std::vector<float> &sigma, std::vector<float> &V) = 0;

	SVD_Algo(const std::vector <float> &m, size_t rr, size_t cc) : matrix(m), rows(rr), columns(cc)
	{}

	SVD_Algo() = delete;

	SVD_Algo(const SVD_Algo &algo) = default;

	SVD_Algo(SVD_Algo &&algo)
	{
		rows = algo.rows;
		columns = algo.columns;
		matrix = std::move(algo.matrix);
	}

	virtual ~SVD_Algo()
	{
		matrix.clear();
		matrix.shrink_to_fit();
	}
};