#define SVD_EXPORT

#include "SVD.h"

void SVD(	const std::vector <float> &matrix,
			size_t rows,
			size_t columns,
			std::vector <float> &U,
			std::vector <float> &sigma,
			std::vector <float> &V,
			Algo algo_type)
{

}

void solve_equation(	const std::vector <float> &matrix,
						const std::vector <float> &vec,
						size_t rows,
						size_t columns,
						std::vector<float> &solution,
						Algo algo_type)
{

}