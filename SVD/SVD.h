#ifdef __linux
	#define SVD_API
#else
	#ifdef SVD_EXPORT
		#define	SVD_API extern "C" __declspec(dllexport)
	#else
		#define SVD_API extern "C" __declspec(dllimport)
	#endif
#endif

#include <vector>

enum Algo
{
	SVD_CPU,
	SVD_GPU
};

SVD_API void SVD(	const std::vector <float> &matrix, 
					size_t rows,
					size_t columns,
					std::vector <float> &U, 
					std::vector <float> &sigma, 
					std::vector <float> &V,
					Algo algo_type);

SVD_API void solve_equation(	const std::vector <float> &matrix,
								const std::vector <float> &vec,
								size_t rows,
								size_t columns,
								std::vector<float> &solution,
								Algo algo_type);