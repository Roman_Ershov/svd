#include "SVD_CPU.h"
#include <numeric>
#include <algorithm>


SVD_CPU::SVD_CPU(const std::vector <float> &m, size_t rr, size_t cc) : SVD_Algo(m, rr, cc)
{}


SVD_CPU::~SVD_CPU()
{
}

void SVD_CPU::perform_svd(std::vector<float> &U, std::vector<float> &sigma, std::vector<float> &V)
{
	
}

void SVD_CPU::householder(std::vector <float> &h_left, std::vector <float> &h_right, std::vector <float> &bidiagonal)
{
	//h_left.resize()
}

void SVD_CPU::matrix_transpose(std::vector <float> &m, std::vector <float> &m_t, size_t N, size_t M)
{

}

float SVD_CPU::norm(const std::vector <float> &vec)
{
	float nrm = std::accumulate(	vec.begin(), vec.end(), 0.0f, 
									[](float a, float b)
									{
										return a * a + b * b;
									});
	return std::sqrtf(nrm);
}

void SVD_CPU::householder_matrix(const std::vector <float> &vec, std::vector <float> &householder_matrix)
{
	householder_matrix.resize(vec.size() * vec.size());
	std::vector <float> u_vec = vec;
	*(u_vec.begin()) += norm(u_vec);
	float norm_u = norm(u_vec);
	std::transform(u_vec.begin(), u_vec.end(), u_vec.begin(), 
					[norm_u](float a)
					{
						return a / norm_u;
					});

	for (size_t row = 0; row < vec.size(); row++)
	{
		for (size_t col = 0; col < vec.size(); col++)
		{
			householder_matrix[row * vec.size() + col] = - 2.0f * u_vec[row] * u_vec[col];
			if (row == col) householder_matrix[row * vec.size() + col] += 1.0;
		}
	}
}