#pragma once
#include "SVD_Algo.h"

class SVD_CPU : public SVD_Algo
{
	void householder(std::vector <float> &h_left, std::vector <float> &h_right, std::vector <float> &bidiagonal);
	void matrix_transpose(std::vector <float> &m, std::vector <float> &m_t, size_t N, size_t M);
	void householder_matrix(const std::vector <float> &vec, std::vector <float> &householder_matrix);
	float norm(const std::vector <float> &vec);

public:
	SVD_CPU(const std::vector <float> &m, size_t rr, size_t cc);
	~SVD_CPU();

	void perform_svd(std::vector<float> &U, std::vector<float> &sigma, std::vector<float> &V);
};

